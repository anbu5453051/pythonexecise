info = input()
info_list = [case.split(',') for case in info.split(' ')] 
print(sorted(info_list))





"""Using multiple iterators, only works for a sequence of 
sequences, whereas just one sequence won't work.

Why doesn't it work with a sequence, 
it will try to unpack the singular element, 
i.e 1, which can't be unpacked as it isn't 
an iterable with 3 elements, that's 
the main reason it works for i.e [1, 2, 3].

"""