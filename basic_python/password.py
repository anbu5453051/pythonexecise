def isvalidpassword(password):
    if(len(password)>6 and len(password)<12):
        lower=False
        upper=False
        num=False
        special=False
        for ch in password:
            if(ch.isdigit()):
                num=True
            if(ch.islower()):
                lower=True
            if(ch.isupper()):
                upper=True
            if(ch.isalnum()):
                special=True
        return num and lower and upper and special                  
    else:
        return False
user=input()
print(isvalidpassword(user))    
